<?php
//操作日志
namespace app\admin\controller;
class OperationLog extends Common{
    //日志列表
    public function logList(){
        $content = $this->request->get('content');
        $oper_member = $this->request->get('oper_member');
        $create_time = $this->request->get('create_time');
        $status = $this->request->get('status');
        $map = [];
        if($content){
            $map[] = ['content','like',"%$content%"];
        }
        if($oper_member){
            $map[] = ['oper_member','like',"%$oper_member%"];
        }
        if($create_time){
            $crtime = explode('~',$create_time);
            $start = $crtime[0];
            $end = $crtime[1];
            //var_dump($start);var_dump($end);
            $map[] = ['create_time','between',[$start,$end]];
        }
        if($status){
            $map[] = ['status','like',"%$status%"];
        }
        $list = db('log')->where($map)->order('create_time desc')->paginate(10,false,['query' => request()->param(),'type' => 'page\Page','var_page'  => 'page']);
        $page = $list->render();
        $this->assign('list',$list);
        $this->assign('page',$page);
        return $this->fetch();
    }
}
