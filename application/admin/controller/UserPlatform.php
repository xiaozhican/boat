<?php
//用户平台管理
namespace app\admin\controller;
use app\admin\model\Information;
use app\admin\model\Notice;
use app\admin\model\Img;
use app\admin\model\Personnel;
use app\admin\model\Record;
use app\admin\service\InformationService;
use app\admin\service\UserPlatformService;
use app\admin\traits\Result;
use think\facade\Cookie;
use think\Request;

class UserPlatform extends Common{
    //用户管理列表
    public function userMsg(){
        $owner_name = $this->request->get('owner_name');//船主姓名
        //var_dump($owner_name);
        $phone = $this->request->get('phone');//联系电话
        $card_number = $this->request->get('card_number');//身份证号
        $now_address = $this->request->get('now_address');//用户住址
        $ship_registry = $this->request->get('ship_registry');//船籍
        $map = [];
        if($owner_name){
            $map[] = ['owner_name','like',"%$owner_name%"];
        }
        if($phone){
            $map[] = ['phone','like',"%$phone%"];
        }
        if($card_number){
            $map[] = ['card_number','like',"%$card_number%"];
        }
        if($now_address){
            $map[] = ['now_address','like',"%$now_address%"];
        }
        $list = db('record')->where($map)->where('is_del',0)->order('status',0)->order('create_time desc')->paginate(10,false,['query' => request()->param(),'type' => 'page\Page','var_page'  => 'page']);
        $page = $list->render();
        $this->assign('list',$list);
        $this->assign('page',$page);
        return $this->fetch();
    }
    //添加和编辑用户
    public function msgedit(){
        if ($this->request->isPost()) {
            $data = $this->request->post();
            if ($data['id']) {
                //编辑
                $res = UserPlatformService::edit($data);
                return $res;
            } else {
                //添加
                $data = UserPlatformService::add($data);
                return $data;
            }
        } else {
            $id = $this->request->get('id', 0, 'intval');
            $list['status'] = 0;
            $list['ship_image'] = '';
            $list['card_front_image'] = '';
            $list['card_back_image'] = '';
            $list['owner_image'] = '';
            $list['id'] = '';
            if ($id) {
                $list = Record::where('id', '=', $id)->find();
                $this->assign('list', $list);
            }else{

                $this->assign('list', $list);
            }
            $this->assign('uid',$this->uid);
            return $this->fetch();
        }
    }
    //驳回
    public function reason(){
        if($this->request->isPost()){
            $data = $this->request->post();
            $r = db('record')->where('id',$data['id'])->update([
                'reason'=>$data['reason'],
                'status'=>2
            ]);
            $user = db('user')->where('uid',$this->uid)->field('user')->find();
            if($r){
                db('log')->insert([
                    'content'=>'船主信息驳回',
                    'oper_member'=>$user['user'],
                    'status'=>'驳回成功',
                    'create_time'=>date('Y-m-d H:i:s')
                ]);
                $msg = Result::success('驳回成功','/admin/userMsg');
            }else{
                db('log')->insert([
                    'content'=>'船主信息驳回',
                    'oper_member'=>$user['user'],
                    'status'=>'驳回失败',
                    'create_time'=>date('Y-m-d H:i:s')
                ]);
                $msg = Result::error('驳回失败');
            }
            return $msg;
        }
    }
    //船主信息删除
    public function usermsgdel(){
        $id = $this->request->param('id', 0, 'intval');
        if ($id) {
            $res = UserPlatformService::delete($id,$this->uid);
            return $res;
        } else {
            $this->error('参数错误');
        }
    }
    public function msgedit1(){
        //global $data;
        if(\think\facade\Request::isPost()){
            $file1 = request()->file('pic1');
            $info = $file1->validate(['size'=>1024*1024*2,'ext'=>'jpg,png,gif,jpeg'])->move( '../public/uploads');
            if($info){
                $path1 = \think\facade\Request::domain().'/uploads/'.$info->getSaveName();
                Cookie::set('pic1',[$path1]);

//                db('img')->where('img_name','船舶照片')->delete();
//                db('img')->insert([
//                    'img_path'=>$path1,
//                    'img_name'=>'船舶照片'
//                ]);
                $msg=['code'=>0,'msg'=>'上传成功'];
            }else{
                $msg=['code'=>1,'msg'=>$file1->getError()];
            }
            return $msg;
        }
    }
    //船主身份证正面照
    public function msgedit2(){
        if(\think\facade\Request::isPost()){
            $file2 = request()->file('pic2');
            $data = $this->request->post();
            $user = db('user')->where('uid',$this->uid)->field('user')->find();
            //var_dump($data['id']);die;
            $info = $file2->validate(['size'=>1024*1024*2,'ext'=>'jpg,png,gif,jpeg'])->move( '../public/identify/front');
            if($info){
                $path2 = '/identify/front/'.$info->getSaveName();
                //如果是添加船主信息，则上传
                if(empty($data)){
                    $is_card = db('img')->where('img_name','船主身份证正面照')->find();
                    if(empty($is_card)){
                        db('img')->insert([
                            'img_path'=>$path2,
                            'img_name'=>'船主身份证正面照'
                        ]);
                        db('log')->insert([
                            'content'=>'上传船主身份证正面照',
                            'oper_member'=>$user['user'],
                            'status'=>'上传成功',
                            'create_time'=>date('Y-m-d H:i:s')
                        ]);
                    }else{
                        db('img')->where('img_name','船主身份证正面照')->update([
                            'img_path'=>$path2
                        ]);
                    }
                }else{
                    //否则更新船主身份证正面照
                    db('record')->where('id',$data['id'])->update([
                        'card_front_image'=>$path2
                    ]);
                    db('log')->insert([
                        'content'=>'更新船主身份证正面照，id为：'.$data['id'],
                        'oper_member'=>$user['user'],
                        'status'=>'更新成功',
                        'create_time'=>date('Y-m-d H:i:s')
                    ]);
                }
                $msg=['code'=>0,'msg'=>'上传成功'];
            }else{
                $msg=['code'=>1,'msg'=>$file2->getError()];
            }
            return $msg;
        }
    }
    //船主身份证反面照
    public function msgedit3(){
        if(\think\facade\Request::isPost()){
            $file3 = request()->file('pic3');
            $data = $this->request->post();
            $user = db('user')->where('uid',$this->uid)->field('user')->find();
            $info = $file3->validate(['size'=>1024*1024*2,'ext'=>'jpg,png,gif,jpeg'])->move( '../public/identify/back');
            if($info){
                $path3 = '/identify/back/'.$info->getSaveName();
                //如果是添加船主信息，则上传
                if(empty($data)){
                    $is_card = db('img')->where('img_name','船主身份证反面照')->find();
                    if(empty($is_card)){
                        db('img')->insert([
                            'img_path'=>$path3,
                            'img_name'=>'船主身份证反面照'
                        ]);
                        db('log')->insert([
                            'content'=>'上传船主身份证反面照',
                            'oper_member'=>$user['user'],
                            'status'=>'上传成功',
                            'create_time'=>date('Y-m-d H:i:s')
                        ]);
                    }else{
                        db('img')->where('img_name','船主身份证反面照')->update([
                            'img_path'=>$path3
                        ]);
                    }
                }else{
                    //否则更新船主身份证正面照
                    db('record')->where('id',$data['id'])->update([
                        'card_back_image'=>$path3
                    ]);
                    db('log')->insert([
                        'content'=>'更新船主身份证反面照，id为：'.$data['id'],
                        'oper_member'=>$user['user'],
                        'status'=>'更新成功',
                        'create_time'=>date('Y-m-d H:i:s')
                    ]);
                }
                $msg=['code'=>0,'msg'=>'上传成功'];
            }else{
                $msg=['code'=>1,'msg'=>$file3->getError()];
            }
            return $msg;
        }
    }
    public function msgedit4(){
        if(\think\facade\Request::isPost()){
            $file4 = request()->file('pic4');
            $info = $file4->validate(['size'=>1024*1024*2,'ext'=>'jpg,png,gif,jpeg'])->move( '../public/uploads');
            if($info){
                $path4 = \think\facade\Request::domain().'/uploads/'.$info->getSaveName();
                db('img')->where('img_name','船主照片')->delete();
                db('img')->insert([
                    'img_path'=>$path4,
                    'img_name'=>'船主照片'
                ]);
                $msg=['code'=>0,'msg'=>'上传成功'];
            }else{
                $msg=['code'=>1,'msg'=>$file4->getError()];
            }
            return $msg;
        }
    }
    //资讯管理
    public function notice(){
        $title = $this->request->get('title');//标题
        $create_time = $this->request->get('create_time');//创建时间
        $map = [];
        if($title){
            $map[] = ['title','like',"%$title%"];
        }
        if($create_time){
            $create_time = explode('~',$create_time);
            $start = $create_time[0];
            $end = $create_time[1];
            $map[] = ['create_time','between',[strtotime($start),strtotime($end)]];
        }
        $list = db('information')->where($map)->where('is_del',0)->order('create_time desc')->paginate(10,false,['query' => request()->param(),'type' => 'page\Page','var_page'  => 'page']);
        $page = $list->render();
        $this->assign('list',$list);
        $this->assign('page',$page);
        return $this->fetch();
    }
    //添加和编辑资讯管理
    public function noticeedit(){
        if ($this->request->isPost()) {
            $data = $this->request->post();
            //$notiecImg = $this->request->file('owner_info');
            //var_dump($data);
            //var_dump($notiecImg);
            //die;
            if ($data['id']) {
                //编辑
                $res = InformationService::edit($data);
                return $res;
            } else {
                //添加
                $data = InformationService::add($data);
                return $data;
            }
        } else {
            $id = $this->request->get('id', 0, 'intval');
            if ($id) {
                $list = Information::where('id', '=', $id)->find();
                //$list['group_id'] = AuthGroupAccess::where('uid', '=', $uid)->column('group_id');
                $this->assign('list', $list);
            }
            $this->assign('uid',$this->uid);
            return $this->fetch();
        }
    }
    //资讯批量删除
    public function delall(){
        if($this->request->isPost()){
            $data = $this->request->post();
            $map[] = ['id','in',$data['id']];
            $query = db('notice')->where($map)->update(['is_del'=>1]);
            if($query){
                $user = db('user')->where('uid',$this->uid)->field('user')->find();
                db('log')->insert([
                    'content'=>'批量删除资讯信息，id为：'.$data['id'],
                    'oper_member'=>$user['user'],
                    'status'=>'删除成功',
                    'create_time'=>date('Y-m-d H:i:s')
                ]);
                return json(['status'=>0,'msg'=>'删除成功','data'=>$query]);
            }else{
                return json(['status'=>-1,'msg'=>'删除失败']);
            }
        }
    }
    //资讯删除
    public function noticedel(){
        $id = $this->request->param('id', 0, 'intval');
        if ($id) {
            $res = InformationService::delete($id,$this->uid);
            return $res;
        } else {
            $this->error('参数错误');
        }
    }
    //资讯图片
    public function uploadnoticeimg(){
        if($this->request->isPost()){
            $notiecImg = $this->request->file('information_info');
            //var_dump($notiecImg);return false;
            if ($notiecImg) //船主相片
            {
                $imageList = [];

                foreach ($notiecImg as $val)
                {
                    $file = $val -> move('notice'); // 将文件移动到指定位置
                    array_push($imageList,'/notice/'.$file -> getSaveName());
                }

                //$update['owner_image'] = serialize($imageList); // 图片数组序列化
                $imgs = serialize($imageList); // 图片数组序列化
                $notimg = db('img')->where('img_name','资讯图片')->find();
                if(!$notimg){
                    db('img')->insert([
                        'img_path'=>$imgs,
                        'img_name'=>'公告图片'
                    ]);
                }else{
                    db('img')->where('img_name','资讯图片')->update([
                        'img_path'=>$imgs
                    ]);
                }
                $this -> result(null,1,'上传成功');
            }
        }
    }

    //banner管理
    public function banner(){
        //获取轮播图
        $list=db('img')->order('sort')->select();

        $this->assign('list',$list);
        return $this->fetch();
    }

    //banner添加
    public function addbanner(){
        $img = new Img();
        if ($this->request->isPost()) {
            if($this->request->isPost()){

                $sort = input('sort');
                $img_name = input('img_name');
                $notiecImg = $this->request->file('information_info');
                if ($notiecImg) //船主相片
                {
                    //$imageList = [];
                    $imageList = '';
                    foreach ($notiecImg as $val)
                    {
                        $file = $val -> move('notice'); // 将文件移动到指定位置
                        //array_push($imageList,'/notice/'.$file -> getSaveName());
                        $imageList='/notice/'.$file -> getSaveName();
                    }
                    //$imgs = serialize($imageList); // 图片数组序列化
                    $img->save([
                        'img_path'=>$imageList,
                        'img_name'=>$img_name,
                        'sort'=>$sort
                    ]);
                    //添加日志
                    $user = db('user')->where('uid',$this->uid)->field('user')->find();
                    db('log')->insert([
                        'content'=>'新增轮播图，id为：'.$img->id,
                        'oper_member'=>$user['user'],
                        'status'=>'上传成功',
                        'create_time'=>date('Y-m-d H:i:s')
                    ]);
                    $this -> result(null,1,'上传成功');
                }
            }
        }else{
            $id = $this->request->get('id', 0, 'intval');
            if ($id) {
                $list = db('img')->where('id', '=', $id)->find();
                $this->assign('list', $list);
            }
            return $this->fetch();
        }
    }

    //banner删除
    public function delimg(){
        $id = $this->request->param('id', 0, 'intval');
        if ($id) {
            db('img')->where('id',$id)->delete();
            //添加日志
            $user = db('user')->where('uid',$this->uid)->field('user')->find();
            db('log')->insert([
                'content'=>'删除轮播图，id为：'.$id,
                'oper_member'=>$user['user'],
                'status'=>'删除成功',
                'create_time'=>date('Y-m-d H:i:s')
            ]);
            $result=[
                'code' => 200,
                'msg'  => '删除成功',
            ];
            return $result;
        } else {
            $this->error('参数错误');
        }
    }

    //公告管理
    public function noticegl(){
        //获取公告列表分页
        $list_notice = db('notice')->where(['is_del'=>0])->order('create_time desc')->paginate(10,false,['query' => request()->param(),'type' => 'page\Page','var_page'  => 'page']);
        $page = $list_notice->render();

        $this->assign('list_notice',$list_notice);
        $this->assign('page',$page);
        return $this->fetch();
    }

    //公告添加
    public function addnotice(){
        $notice = new Notice();
        if ($this->request->isPost()) {
            $data = $this->request->post();
            if ($data['id']) {
                //编辑
                $data['cid']=$this->uid;
                $data['update_time']= time();

                $res_up = $notice->where(['id'=>$data['id']])->update($data);
                //添加日志
                $user = db('user')->where('uid',$this->uid)->field('user')->find();
                db('log')->insert([
                    'content'=>'编辑公告，id为：'.$data['id'],
                    'oper_member'=>$user['user'],
                    'status'=>'编辑成功',
                    'create_time'=>date('Y-m-d H:i:s')
                ]);

                if($res_up){
                    $msg = Result::success('修改成功', url('/admin/banner'));
                }else{
                    $msg = Result::success('修改失败', url('/admin/banner'));
                }
                return json($msg);
            } else {
                //添加
                $data['cid']=$this->uid;
                $data['create_time']= time();
                $data['is_del']=0;

                $res = $notice->save($data);
                //添加日志
                $user = db('user')->where('uid',$this->uid)->field('user')->find();
                db('log')->insert([
                    'content'=>'新增公告，id为：'.$notice->id,
                    'oper_member'=>$user['user'],
                    'status'=>'新增成功',
                    'create_time'=>date('Y-m-d H:i:s')
                ]);

                if($res){
                    $msg = Result::success('添加成功', url('/admin/banner'));
                }else{
                    $msg = Result::success('添加失败', url('/admin/banner'));
                }
                return json($msg);
            }
        }else{
            $id = $this->request->get('id', 0, 'intval');
            if ($id) {
                $list = $notice->where('id', '=', $id)->find();
                $this->assign('list', $list);
            }
            return $this->fetch();
        }
    }

    //公告删除
    public function delnotice(){
        $id = $this->request->param('id', 0, 'intval');
        if ($id) {
            $notice = new Notice();
            $data=[];
            $data['is_del']=1;
            $res=$notice->where('id',$id)->update($data);
            $user = db('user')->where('uid',$this->uid)->field('user')->find();
            //添加日志
            db('log')->insert([
                'content'=>'删除公告，id为：'.$id,
                'oper_member'=>$user['user'],
                'status'=>'删除成功',
                'create_time'=>date('Y-m-d H:i:s')
            ]);
            if($res){
                $result=[
                    'code' => 200,
                    'msg'  => '删除成功',
                ];
            }else{
                $result=[
                    'code' => 250,
                    'msg'  => '删除失败',
                ];
            }
            return $result;
        } else {
            $this->error('参数错误');
        }
    }
}
