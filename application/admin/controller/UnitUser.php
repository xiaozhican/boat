<?php
namespace app\admin\controller;
class UnitUser extends Common{
    //单位成员列表
    public function unitUser(){
        return $this->fetch();
    }
    //新增和编辑单位成员
    public function unitedit(){
        if ($this->request->isPost()) {
            $data = $this->request->post();
            if ($data['uid']) {
                //编辑
                $res = UserService::edit($data);
                return $res;
            } else {
                //添加
                $data = UserService::add($data);
                return $data;
            }
        } else {
            $uid = $this->request->get('uid', 0, 'intval');
            if ($uid) {
                $list = UserModel::where('uid', '=', $uid)->hidden(['password'])->find();
                $list['group_id'] = AuthGroupAccess::where('uid', '=', $uid)->column('group_id');
                $this->assign('list', $list);
            }
            return $this->fetch();
        }
    }
}
