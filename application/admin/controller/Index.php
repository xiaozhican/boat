<?php
/**
 * Created by originThink
 * Author: 原点 467490186@qq.com
 * Date: 2018/1/16
 * Time: 15:24
 */
namespace app\admin\controller;

use app\admin\model\Report;
use app\admin\service\ReportService;
use app\admin\traits\Result;
use auth\Auth;
use app\admin\model\User;
use app\admin\model\Config;
use think\config\driver\Json;

class Index extends Common
{
    /**
     * 首页
     * @return mixed
     * @author 原点 <467490186@qq.com>
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function index()
    {
        //获取菜单
        $menuList = (new Auth($this->uid, $this->group_id))->getMenuList();
        $this->assign('menuList', $menuList);
        $info = User::get($this->uid)->hidden(['password']);
        $info['head'] ? : $info['head'] = '/images/face.jpg';
        $this->assign('info', $info);
        //公告
        $notice_config = $this->noticeConfig();
        $this->assign('notice_config', $notice_config);
        return $this->fetch();
    }

    /**
     * layui 首页
     * @return mixed
     * @author 原点 <467490186@qq.com>
     */
    public function home()
    {
        $status = $this->request->get('status');//状态
        $ship_number = $this->request->get('ship_number');//船名船号
        $ship_name = $this->request->get('ship_name');//船主姓名
        $ship_phone = $this->request->get('ship_phone');//船主电话
        $clearance_time = $this->request->get('clearance_time');//船主姓名
        $map = [];
        if($status != ''){
            $map[] = ['status','=',$status];
        }
        if($ship_number){
            $map[] = ['ship_number','like',"%$ship_number%"];
        }
        if($ship_name){
            $map[] = ['ship_name','like',"%$ship_name%"];
        }
        if($ship_phone){
            $map[] = ['ship_phone','like',"%$ship_phone%"];
        }
        if($clearance_time){
            $clearance_time = explode('~',$clearance_time);
            //var_dump($clearance_time[0]);
            //var_dump($clearance_time[1]);
            $start = $clearance_time[0];
            $end = $clearance_time[1];
            $map[] = ['clearance_time','between',[strtotime($start),strtotime($end)]];
        }
        $list = db('report')->where($map)->where('isdelete',0)->order('status',0)->order('create_time desc')->paginate(10,false,['query' => request()->param(),'type' => 'page\Page','var_page'  => 'page']);
        $page = $list->render();
        $this->assign('list',$list);
        $this->assign('page',$page);
        return $this->fetch();
    }
    //出入港报备信息审核
    public function checkedit()
    {
        if ($this->request->isPost()) {
            $data = $this->request->post();
            //var_dump($data);die;
            if ($data['id']) {
                //编辑
                $res = ReportService::edit($data);
                return $res;
            } else {
                //添加
                $data = ReportService::add($data);
                return $data;
            }
        } else {
            $id = $this->request->get('id', 0, 'intval');
            if ($id != 0) {
                $list = Report::where('id', '=', $id)->find();
                $list['clearance_time'] = date('Y-m-d H:i:s',$list['clearance_time']);
                $list['return_time'] = date('Y-m-d H:i:s',$list['return_time']);
                $this->assign('list', $list);
            }
            $this->assign('uid',$this->uid);
            return $this->fetch('index/checkedit');
        }
    }
    //出入港信息删除
    public function homedel(){
        $id = $this->request->param('id', 0, 'intval');
        if ($id) {
            $res = ReportService::delete($id,$this->uid);
            return $res;
        } else {
            $this->error('参数错误');
        }
    }
    //出入港驳回
    public function homereason(){
        if($this->request->isPost()){
            $data = $this->request->post();
            $r = db('report')->where('id',$data['id'])->update([
                'reason'=>$data['reason'],
                'status'=>2
            ]);
            $user = db('user')->where('uid',$this->uid)->field('user')->find();
            if($r){
                db('log')->insert([
                    'content'=>'出入港报备信息，id为：'.$data['id'],
                    'oper_member'=>$user['user'],
                    'status'=>'驳回成功',
                    'create_time'=>date('Y-m-d H:i:s')
                ]);
                $msg = Result::success('驳回成功','/admin/userMsg');
            }else{
                db('log')->insert([
                    'content'=>'出入港报备信息，id为：'.$data['id'],
                    'oper_member'=>$user['user'],
                    'status'=>'驳回失败',
                    'create_time'=>date('Y-m-d H:i:s')
                ]);
                $msg = Result::error('驳回失败');
            }
            return $msg;
        }
    }

    /**
     * 公告配置信息
     * @return array|mixed|\PDOStatement|string|\think\Model|null
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    private function noticeConfig()
    {
        $notice_config = cache('notice_config');
        if ($notice_config) {
            return $notice_config;
        }
        $list = Config::where('name', '=', 'notice_config')->field('value')->find();
        cache('notice_config', $list);
        return $list;
    }
}
