<?php
/**
 * Created by originThink
 * Author: 原点 467490186@qq.com
 * Date: 2018/9/7
 * Time: 10:00
 */

namespace app\admin\service;

use app\admin\model\Information;
use app\admin\model\Notice;
use app\admin\model\Personnel;
use app\admin\model\Record;
use app\admin\model\Report;
use app\admin\model\User;
use app\admin\model\LoginLog;
use think\facade\Request;
use app\admin\traits\Result;

class InformationService
{
    use Result;

    /**
     * 添加资讯
     * @param $data
     * @return array
     * @author 原点 <467490186@qq.com>
     * @throws \Exception
     */
    public static function add($data)
    {
        $notice = new Information();
        $notice->cid = $data['uid'];
        $notice->title = $data['title'];
        $notice->content = $data['content'];
        $notice->create_time = time();
        $notice->update_time = time();
        $res = $notice->save();
        if ($res) {
            $user = db('user')->where('uid',$data['uid'])->field('user')->find();
            db('log')->insert([
                'content'=>'添加资讯信息，id为：'.$notice->id,
                'oper_member'=>$user['user'],
                'status'=>'添加成功',
                'create_time'=>date('Y-m-d H:i:s')
            ]);
            $msg = Result::success('添加成功', url('/admin/notice'));
        }else{
            $msg = Result::error('添加失败');
        }
        return $msg;
    }

    /**
     * 编辑公告
     * @param $data
     * @return array|string
     * @author 原点 <467490186@qq.com>
     * @throws \Exception
     */
    public static function edit($data)
    {
        $noticedata = [
            'title' => $data['title'],
            'content' => $data['content'],
            'cid'=>$data['uid'],
            'update_time' => time(),
        ];
        $res = Information::update($noticedata, ['id' => $data['id']]);
        $user = db('user')->where('uid',$data['uid'])->field('user')->find();
        if ($res) {
            db('log')->insert([
                'content'=>'编辑资讯信息，id为：'.$data['id'],
                'oper_member'=>$user['user'],
                'status'=>'编辑成功',
                'create_time'=>date('Y-m-d H:i:s')
            ]);
            $msg = Result::success('编辑成功', url('/admin/home'));
        } else {
            db('log')->insert([
                'content'=>'编辑资讯公告，id为：'.$data['id'],
                'oper_member'=>$user['user'],
                'status'=>'编辑失败',
                'create_time'=>date('Y-m-d H:i:s')
            ]);
            $msg = Result::error('编辑失败');
        }
        return $msg;
    }

    /**
     * 删除资讯公告
     * @param $uid 用户id
     * @return array|string
     * @author 原点 <467490186@qq.com>
     * @throws \Exception
     */
    public static function delete($id,$uid)
    {
        $res = db('information')->where('id',$id)->update([
            'is_del'=>1,
            'update_time'=>time()
        ]);
        if ($res) {
            $user = db('user')->where('uid',$uid)->field('user')->find();
            db('log')->insert([
                'content'=>'资讯信息删除，id为：'.$id,
                'oper_member'=>$user['user'],
                'status'=>'删除成功',
                'create_time'=>date('Y-m-d H:i:s')
            ]);
            $msg = Result::success('删除成功');
        } else {
            $msg = Result::error('删除失败');
        }
        return $msg;
    }

}