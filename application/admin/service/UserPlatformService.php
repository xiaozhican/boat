<?php
/**
 * Created by originThink
 * Author: 原点 467490186@qq.com
 * Date: 2018/9/7
 * Time: 10:00
 */

namespace app\admin\service;

use app\admin\model\Personnel;
use app\admin\model\Record;
use app\admin\model\User;
use app\admin\model\LoginLog;
use think\facade\Request;
use app\admin\traits\Result;

class UserPlatformService
{
    use Result;

    /**
     * 添加用户
     * @param $data
     * @return array
     * @author 原点 <467490186@qq.com>
     * @throws \Exception
     */
    public static function add($data)
    {
        $condition = [];
        $personnel = new Personnel();
        $personnel->member_name = $data['member_name'];
        $personnel->phone = $data['phone'];
        $personnel->card_number = $data['card_number'];
        $personnel->now_address = $data['now_address'];
        $personnel->register_address = $data['register_address'];
        $personnel->exigency_name = $data['exigency_name'];
        $personnel->exigency_phone = $data['exigency_phone'];
        $personnel->ship_num = $data['ship_num'];
        $personnel->ship_color = $data['ship_color'];
        $personnel->tonnage = $data['tonnage'];
        $personnel->ship_port = $data['ship_port'];
        $personnel->rang_type = $data['rang_type'];
        $personnel->ship_port = $data['ship_port'];

        $personnel->ship_img = $condition['ship_img'];
        $personnel->member_img = $condition['member_img'];
        $personnel->member_ship = $condition['member_ship'];
        //$personnel->reject = $data['reject'];
        $personnel->create_time = time();
        $res = $personnel->save();
        if ($res) {
            $msg = Result::success('添加成功', url('/admin/userMsg'));
        }else{
            $msg = Result::error('添加失败');
        }
        return $msg;
    }

    /**
     * 编辑用户
     * @param $data
     * @return array|string
     * @author 原点 <467490186@qq.com>
     * @throws \Exception
     */
    public static function edit($data)
    {
        $recorddata = [
            'ship_name' => $data['ship_name'],
            'owner_name' => $data['owner_name'],
            'phone' => $data['phone'],
            'now_address' => $data['now_address'],
            'register_address' => $data['register_address'],
            'tonnage' => $data['tonnage'],
            'ship_registry' => $data['ship_registry'],
            'exigency_name' => $data['exigency_name'],
            'exigency_phone' => $data['exigency_phone'],
            'status'=>1,
            'sid'=>$data['uid'],
            'audit_time' => time(),
        ];
        $res = Record::update($recorddata, ['id' => $data['id']]);
        $user = db('user')->where('uid',$data['uid'])->field('user')->find();
        if ($res) {
            db('log')->insert([
                'content'=>'船主信息审核，id为：'.$data['id'],
                'oper_member'=>$user['user'],
                'status'=>'审核通过',
                'create_time'=>date('Y-m-d H:i:s')
            ]);
            $msg = Result::success('审核成功', url('/admin/userMsg'));
        } else {
            db('log')->insert([
                'content'=>'船主信息审核，id为：'.$data['id'],
                'oper_member'=>$user['user'],
                'status'=>'审核失败',
                'create_time'=>date('Y-m-d H:i:s')
            ]);
            $msg = Result::error('审核失败');
        }
        return $msg;
    }

    /**
     * 删除用户
     * @param $uid 用户id
     * @return array|string
     * @author 原点 <467490186@qq.com>
     * @throws \Exception
     */
    public static function delete($id,$uid)
    {
        $res = db('record')->where('id',$id)->update([
            'is_del'=>1
        ]);
        $user = db('user')->where('uid',$uid)->field('user')->find();
        if ($res) {
            db('log')->insert([
                'content'=>'船主信息删除，id为：'.$id,
                'oper_member'=>$user['user'],
                'status'=>'删除成功',
                'create_time'=>date('Y-m-d H:i:s')
            ]);
            $msg = Result::success('删除成功');
        } else {
            db('log')->insert([
                'content'=>'船主信息删除，id为：'.$id,
                'oper_member'=>$user['user'],
                'status'=>'删除失败',
                'create_time'=>date('Y-m-d H:i:s')
            ]);
            $msg = Result::error('删除失败');
        }
        return $msg;
    }

}