<?php


namespace app\sys\controller;


use app\index\model\Feedback;
use app\utils\Constants;
use app\utils\SmsUtil;
use think\Controller;
use think\facade\Cache;
use think\Facade\Session;

class System extends Controller
{
    /**
     * 发送验证码
     * Created On 2019/9/26 9:42
     * Created By Am.
     */
    public function send()
    {
        $userPhone = input('user_phone');

        if ($userPhone == null)
        {
            $this -> result(null,-1,'手机号不能为空');
        }

        $code = mt_rand(100000,999999); // 生成6位随机码

        $send = SmsUtil::sendSms($userPhone,$code); // 发送
        Cache::store('redis') -> set(Constants::SMS_BELONG.$userPhone,$code,600); // 仅存在十分钟

        if ($send -> Code === 'OK') // 发送成功
        {
            $this -> result(null,1,'发送成功');
        } else {
            $this -> result($send,-1,'发送失败');
        }
    }

    public function feedback()
    {
        if ($this -> request -> isPost())
        {
            $feedback = [];

            $feedbackModel = new Feedback();

            $feedback['content'] = input('content');
            $feedback['user_id'] = Session::get('user_login','front')['id'];
            $feedback['create_time'] = time();
            $feedback['category'] = input('category');

            $fileList = $this -> request -> file('desc_image');

            if ($fileList)
            {
                $imageList = [];

                foreach ($fileList as $val)
                {
                    $file = $val -> move('feedback/'); // 将文件移动到指定位置
                    array_push($imageList,'/feedback/'.$file -> getSaveName());
                }

                $feedback['image_desc'] = serialize($imageList); // 图片数组序列化
            }

            $insert = $feedbackModel -> insert($feedback);

            if ($insert)
            {
                $this -> result(null,1,'我们已收到您的反馈');
            } else {
                $this -> result(null,-1,'反馈失败');
            }
        }

        return $this -> fetch();
    }
}