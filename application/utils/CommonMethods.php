<?php


namespace app\utils;


use think\Exception;

class CommonMethods
{
    /**
     * 异常返回页
     * @param Exception $e
     * @return \think\response\View
     * Created On 2019/9/26 9:23
     * Created By Am.
     */
    static function exceptionPage(Exception $e)
    {
        return view('main/home');
    }

}