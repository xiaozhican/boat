<?php


namespace app\index\controller;


use app\admin\model\Notice;
use app\index\model\Img;

class Main extends Base
{
    public function home()
    {
        // 开始获取公告内容
        $notice = new Notice();
        $noticeList = $notice -> where("is_del = 0") -> select();
        $list = '\'';
        foreach ($noticeList as $item) {
            $list .= htmlspecialchars_decode($item['title']).'\',\'';
        } // 公告

        $list = rtrim($list,'\'');
        $list = rtrim($list,',');

        $this -> assign('noticeList',$list);
        // 公告内容结束

        // 开始获取轮播图
        $imgModel = new Img();
        $banner = $imgModel -> select();
        foreach ($banner as $item)
        {
            $item['img_path'] = '101.37.67.14:86'.$item['img_path'];
        }
        $this -> assign('bannerList', $banner);
        // 轮播图获取结束

        return $this -> fetch();
    }
}