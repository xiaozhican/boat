<?php


namespace app\index\controller;


use think\Controller;
use app\index\model\Report;
use app\index\model\Personnel;
use think\facade\Session;

class Apply extends Controller
{
    public function index()
    {
        if(!isset($_GET['state'])){
            $state=0;
        }else{
            $state=$_GET['state'];
        }

        $where=[];
        $where['uid']=Session::get('user_login','front')['id'];
        $where['status']=$state;
        $where['isdelete']=0;
        $list = Report::where($where)->select()->toArray();
//        if($list){
//            foreach ($list as $k=>$v){
//                var_dump($v['member_info']);
//                //$member_info = unserialize($v['member_info']);
//                //$member_info=json_encode($v['member_info']);
//                $member_info_s=json_decode($v['member_info'],true);
//                var_dump($member_info_s);
//
//                //var_dump($data);
//                //$member_name = trim($member_name,','); //移除首尾的逗号
//               // $list[$k]['cy']=$member_name;
//            }exit;
//        }
        //$list=$list->toArray();
        $this->assign('list', $list);
        $this->assign('state', $state);
        return $this -> fetch();
    }

    /**
    **删除船舶出海申请（不是真的删除）
     */
    public function delete(){
        $id=$_POST['id'];
        $where=[];
        $where['uid']=1;
        $where['id']=$id;
        $del = Report::where($where)->update(['isdelete'=>1]);
        if($del){
            $result=[
                'code' => 200,
                'msg'  => '删除成功',
            ];
        }else{
            $result=[
                'code' => 250,
                'msg'  => '删除失败',
            ];
        }
        return $result;
    }
}