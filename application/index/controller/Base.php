<?php


namespace app\index\controller;


use app\utils\Constants;
use think\App;
use think\Facade\Cache;
use think\Controller;
use think\facade\Request;
use think\Facade\Session;

class Base extends Controller
{

    public function __construct(App $app = null)
    {
        parent::__construct($app);

        $path = Request::url();

        if (!in_array($path,['/home/entrance','/home/boat','/home/idcard']))
        {
            if (!Session::has('user_login','front'))
            {
                $this -> redirect('/home/entrance');
            }
        }
    }

    /**
     * 验证手机号码
     * @param $phoneNumber
     * @param $code
     * Created On 2019/9/26 10:53
     * Created By Am.
     */
    public function checkCode($phoneNumber,$code)
    {
        if (!Cache::store('redis') -> has(Constants::SMS_BELONG.$phoneNumber))
        {
            $this -> result(null,-1,'该号码不存在验证码');
        }

        if (Cache::store('redis') -> get(Constants::SMS_BELONG.$phoneNumber) != $code)
        {
            $this -> result(null,-1,'验证码错误');
        }
    }

    /**
     * 获取页码
     * @param $pageSize
     * @return float|int
     * Created On 2019/9/28 13:54
     * Created By Am.
     */
    function pageInit($pageSize)
    {
        if (input('page') === null || input('page') <= 0)
        {
            return 0;
        }
        return (input('page') - 1) * $pageSize;
    }

    /**
     * 显示尺寸
     * @return int|mixed
     * Created On 2019/9/28 13:54
     * Created By Am.
     */
    function pageSizeInit()
    {
        if (input('page_size') === null || input('page_size') <= 0)
        {
            return 10;
        }
        return input('page_size');
    }
}