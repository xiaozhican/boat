<?php

namespace app\index\controller;

use app\utils\Constants;
use think\App;
use think\Controller;
use think\Facade\Cache;
use app\index\model\UserClient;
use think\facade\Session;

class Login extends Controller
{
    protected $userClient = null;

    public function __construct(App $app = null)
    {
        parent::__construct($app);

        $this -> userClient = new UserClient();
    }

    /**
     * 用户注册
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * Created On 2019/9/26 11:06
     * Created By Am.
     */
    public function register()
    {
        $phoneNumber = input('user_phone');
        $code = input('code');
        $condition = [];
        $condition['mobile_phone'] = strval($phoneNumber);

        $isExist = $this -> userClient -> where($condition) -> find();

        if ($isExist)
        {
            $this -> result(null,-1,'用户已存在，请登陆');
        }

        $this -> checkCode($phoneNumber, $code);

        $save = [];

        $save['mobile_phone'] = $phoneNumber;
        $save['register_ip'] = $this -> request -> ip();
        $save['create_time'] = time();

        $userClientModel = new UserClient();
        $register = $userClientModel -> insert($save);

        if ($register)
        {
            $this -> result(['url' => '/home/boat', 'uid' => $userClientModel -> getLastInsID()],1,'注册成功');
        } else {
            $this -> result(null,-1,'注册失败');
        }
    }

    /**
     * 用户登录
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * Created On 2019/9/26 10:56
     * Created By Am.
     */
	public function login(){
        $phoneNumber = input('user_phone');
        $code = input('code');

        if ($phoneNumber === null)
        {
            $this -> result(null,-1,'手机号不能为空');
        }

        if ($code === null)
        {
            $this -> result(null,-1,'验证码不能为空');
        }

        $condition = [];
        $condition['mobile_phone'] = strval($phoneNumber);

        $isExist = $this -> userClient -> where($condition) -> find();

        if (!$isExist)
        {
            $this -> result(null,-1,'用户不存在，请注册');
        } // 验证用户是否存在

        $this -> checkCode($phoneNumber, $code);

        // 通过验证，立即销毁验证码
        Cache::store('redis') -> rm(Constants::SMS_BELONG.$phoneNumber);

        // 根据 is_complete 判断是否注册完整，否则继续填写船舶信息

        if ($isExist['is_complete'] == 1)
        {
            Session::set('user_login',$isExist,'front');

            $this -> result(['url' => '/home/home?action=login', 'uid' => null],1,'完整注册');
        }

        if ($isExist['is_complete'] == 2)
        {
            $this -> result(['url' => '/home/boat', 'uid' => $isExist['id']],1,'未认证船只信息');
        }

	}



	// --------------------------------------------- 其他方法


    public function checkCode($phoneNumber,$code)
    {
        if (!Cache::store('redis') -> has(Constants::SMS_BELONG.$phoneNumber))
        {
            $this -> result(null,-1,'该号码不存在验证码');
        }

        if (Cache::store('redis') -> get(Constants::SMS_BELONG.$phoneNumber) != $code)
        {
            $this -> result(null,-1,'验证码错误');
        }
    }

	/**
     * 记录session
     * @param $uid int 用户id
     * @param $group_id array 用户组
     * @author 原点 <467490186@qq.com>
     */
    private static function autoSession($uid)
    {
        //获取用户信息
        $user = UserClient::get($uid);
        /* 记录登录SESSION */
        $auth = [
            'uid' => $user['id'],
            'user_name' => $user['user_name'],
        ];
        //设置session
        session('user_auth', $auth);
    }

    /**
     * 用户退出
     * @return array
     * @author 原点 <467490186@qq.com>
     */
    public function logout()
    {
        session('user_auth', null);
        $result=[
            'code' => 200,
            'msg'  => '退出成功',
        ];
        return json($result);
    }
}
