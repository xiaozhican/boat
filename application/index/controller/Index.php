<?php
declare(strict_types = 1);

namespace app\index\controller;

use app\admin\model\Notice;
use app\index\model\Personnel;
use app\index\model\Record;
use app\index\model\UserClient;
use app\utils\CommonMethods;
use think\App;
use think\Controller;
use think\Exception;
use think\facade\Session;

class Index extends Base
{

    protected $recordModel = null;

    public function __construct(App $app = null)
    {
        parent::__construct($app);

        $this -> recordModel = new Record();
    }

    public function index()
    {
        return '<style type="text/css">*{ padding: 0; margin: 0; } div{ padding: 4px 48px;} a{color:#2E5CD5;cursor: pointer;text-decoration: none} a:hover{text-decoration:underline; } body{ background: #fff; font-family: "Century Gothic","Microsoft yahei"; color: #333;font-size:18px;} h1{ font-size: 100px; font-weight: normal; margin-bottom: 12px; } p{ line-height: 1.6em; font-size: 42px }</style><div style="padding: 24px 48px;"> <h1>:) </h1><p> ThinkPHP V5.1<br/><span style="font-size:30px">12载初心不改（2006-2018） - 你值得信赖的PHP框架</span></p></div><script type="text/javascript" src="https://tajs.qq.com/stats?sId=64890268" charset="UTF-8"></script><script type="text/javascript" src="https://e.topthink.com/Public/static/client.js"></script><think id="eab4b9f840753f8e7"></think>';
    }


    /**
     * 登陆注册界面
     * @return mixed|\think\response\View
     * Created On 2019/9/25 16:14
     * Created By Am.
     */
    public function entrance()
    {
        try {
            return $this -> fetch();
        } catch(Exception $exception) {
            return CommonMethods::exceptionPage($exception);
        }
    }

    /**
     * 提交审核，船舶
     * @return mixed
     * Created On 2019/9/26 16:37
     * Created By Am.
     */
    public function boat()
    {
        if ($this -> request -> isPost()) // 获取本页数据
        {
            $save = [];

            $save['uid'] = input('uid');
            $save['owner_name'] = input('owner_name');
            $save['card_number'] = input('card_number');
            $save['register_address'] = input('register_address');
            $save['now_address'] = input('now_address');
            $save['satellite_phone'] = input('satellite_phone');
            $save['radio_station'] = input('radio_station');
            $save['ship_name'] = input('ship_name');
            $save['tonnage'] = input('tonnage');
            $save['ship_registry'] = input('ship_registry');
            $save['exigency_name'] = input('exigency_name');
            $save['exigency_phone'] = input('exigency_phone');
            $save['ship_image'] = $this -> request -> file('ship_image');

            if ($save['ship_image'] !== null) //上传图片
            {
                $imageList = [];

                foreach ($save['ship_image'] as $val)
                {
                     $file = $val -> move('ships/'); // 将文件移动到指定位置
                     array_push($imageList,'/ships/'.$file -> getSaveName());
                }

                $save['ship_image'] = serialize($imageList); // 图片数组序列化
            }

            $result = $this -> recordModel -> insert($save);

            if ($result)
            {
                $this -> result(null,1,'信息提交完成');
            }

            $this -> result(null,-1,'信息提交失败');

        }
        return $this -> fetch();
    }


    /**
     * 提交审核身份证信息
     * @return mixed
     * @throws Exception
     * @throws \think\exception\PDOException
     * Created On 2019/9/26 16:37
     * Created By Am.
     */
    public function idcard()
    {
        if ($this -> request -> isPost())
        {
            $update = [];

            $uid = input('uid');

            $idBack = $this -> request -> file('idBack');
            $idFront = $this -> request -> file('idFront');

            $owner = $this -> request -> file('owner_info');

            if (!$idFront)
            {
                $this -> result(null,-1,'请上传身份证正面照');
            }

            if (!$idBack)
            {
                $this -> result(null,-1,'请上传身份证背面照');
            }

            if (!$owner)
            {
                $this -> result(null,-1,'请上传船主相片');
            }

            if ($uid == null)
            {
                $this -> result(null,-1,'缺少用户标识');
            }
            // 参数部分结束，开始入库

            $where = [];
            $where['uid'] = $uid;

            if ($idFront != null) //身份证正面图片
            {
                $file = $idFront -> move('identify/front/'); // 将文件移动到指定位置

                $update['card_front_image'] = '/identify/front/'.$file -> getSaveName();
            }

            if ($idBack != null) //身份证背面图片
            {
                $file = $idBack -> move('identify/back/'); // 将文件移动到指定位置

                $update['card_back_image'] = '/identify/back/'.$file -> getSaveName();
            }

            if ($owner != null) //船主相片
            {
                $imageList = [];

                foreach ($owner as $val)
                {
                    $file = $val -> move('identify/owner/'); // 将文件移动到指定位置
                    array_push($imageList,'/identify/owner/'.$file -> getSaveName());
                }

                $update['owner_image'] = serialize($imageList); // 图片数组序列化
            }

            $recordModel = new Record();

            $update = $recordModel -> where($where) -> update($update);

            if ($update)
            { // 这边要做另外两部，用户表，船舶信息更新,之后前端跳转至主页
                $userClientModel = new UserClient();

                $info = $userClientModel -> get($uid);

                $condition = [];
                $condition['id'] = $uid;

                Session::set('user_login',$info,'front'); // 登陆session

                // 注册信息完善，船舶信息完善
                $userClientModel -> where($condition) -> update(['is_complete' => 1]); // 用户注册步骤完整
                $recordModel -> where($where) -> update(['is_complete' => 1]); // 船舶信息完整

                $this -> result(null,1,'信息提交成功');
            }

        }
        return $this -> fetch();
    }

    /**
     * 查看成员列表
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * Created On 2019/9/29 9:41
     * Created By Am.
     */
    public function member()
    {
        $personnelModel = new Personnel();

        $info = Session::get('user_login','front');

        $uid = $info['id'];

        $pageSize = $this -> pageSizeInit();

        $page = $this -> pageInit($pageSize);

        $list = $personnelModel -> where("uid = $uid") -> limit($page,$pageSize) -> select();

        $this -> assign('list', $list);

        return $this -> fetch();
    }

    /**
     * 删除成员
     * @throws Exception
     * @throws \think\exception\PDOException
     * Created On 2019/9/29 9:40
     * Created By Am.
     *
     */
    public function del()
    {
        $mid = input('mid');

        $personnel = new Personnel();

        $info = Session::get('user_login','front');

        $uid = $info['id'];

        $condition = [];

        $condition['id'] = $mid;

        $condition['uid'] = $uid;

        $delete = $personnel -> where($condition) -> delete();

        if ($delete)
        {
            $this -> result(null,1,'删除成功');
        }
    }

    /**
     * 新增船员
     * @return mixed
     * Created On 2019/9/29 9:40
     * Created By Am.
     */
    public function add()
    {
        if ($this -> request -> isPost())
        {
            $uid = Session::get('user_login','front')['id'];

            $insert = input();

            $insert['uid'] = $uid;

            $personnel = new Personnel();

            $result = $personnel -> insert($insert);

            if ($result)
            {
                $this -> result(null,1,'新增成功');
            }
        }
        return $this -> fetch();
    }
    //-----------------------------------------------------------------Ajax methods below
}
