<?php


namespace app\index\controller;


use app\index\model\UserClient;
use app\utils\Constants;
use app\utils\SmsUtil;
use think\facade\Cache;
use think\facade\Session;

class Personal extends Base
{
    public function personal()
    {
        $uid = Session::get('user_login','front')['id'];

        $userClient = new UserClient();

        $info = $userClient -> where("id = $uid") -> find();

        $this -> assign('user_name',$info['user_name']);
        $this -> assign('avatar',$info['avatar']);

        return $this -> fetch();
    }

    public function phone()
    {
        return $this -> fetch();
    }

    public function service()
    {
        return $this -> fetch();
    }

    //-------------------------------------ajax method

    /**
     * 更换头像
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     * Created On 2019/9/27 15:48
     * Created By Am.
     */
    public function uploadAvatar()
    {
        $avatar = $this -> request -> file('avatar');

        $update = [];

        if ($avatar != null) //身份证正面图片
        {
            $file = $avatar -> move('user/avatar/'); // 将文件移动到指定位置

            $update['avatar'] = '/user/avatar/'.$file -> getSaveName();
        }

        $uid = Session::get('user_login','front')['id'];

        $userClient = new UserClient();

        $update = $userClient -> where("id = $uid") -> update($update);

        if ($update)
        {
            $this -> result(null,1,'头像更换成功');
        }
    }


    /**
     * 更新用户昵称
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     * Created On 2019/9/27 15:36
     * Created By Am.
     */
    public function updateUserName()
    {
        $userName = input('user_name');

        $userClient = new UserClient();

        $uid = Session::get('user_login','front')['id'];

        $update = $userClient -> where("id = $uid") -> update(['user_name' => $userName]);

        if ($update)
        {
            $this -> result(null,1,'更新成功');
        }
    }

    /**
     * 发送短信验证码
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * Created On 2019/9/27 13:40
     * Created By Am.
     */
    public function getCode()
    {
        $phoneNumber = input('phone_old');

        if ($phoneNumber == null)
        {
            $this -> result(null,-1,'请输入您的电话号');
        }

        $userClient = new UserClient();

        $condition = [];

        $condition['mobile_phone'] = $phoneNumber;

        $isExist = $userClient -> where($condition) -> find();

        if (!$isExist)
        {
            $this -> result(null,-1,'该手机号不存在');
        }

        $code = mt_rand(100000,999999);

        $send = SmsUtil::sendSms($phoneNumber,$code); // 发送
        Cache::store('redis') -> set(Constants::SMS_RESET_PHONE.$phoneNumber,$code,600); // 仅存在十分钟

        if ($send -> Code === 'OK') // 发送成功
        {
            $this -> result(null,1,'发送成功');
        } else {
            $this -> result($send,-1,'发送失败');
        }
    }

    /**
     * 修改用户手机号
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     * Created On 2019/9/27 15:23
     * Created By Am.
     */
    public function updatePhone()
    {
        $oldNumber = input('phone_old');

        $newNumber = input('phone_new');

        $code = input('code');

        if ($oldNumber == null)
        {
            $this -> result(null,-1,'旧手机号码为空');
        }

        if ($newNumber == null)
        {
            $this -> result(null,-1,'新手机号码为空');
        }

        if ($oldNumber == $newNumber)
        {
            $this -> result(null,-1,'新旧号码不能相同');
        }

        $userClient = new UserClient();

        $condition = [];

        $condition['mobile_phone'] = $newNumber;

        $isExistNewNumber = $userClient -> where($condition) -> find(); // 查看新手机号码是否被其他人使用过

        if ($isExistNewNumber)
        {
            $this -> result(null,-1,'新号码已被使用');
        }

        if ($code != Cache::store('redis') -> get(Constants::SMS_RESET_PHONE.$oldNumber))
        {
            $this -> result(null,-1,'验证码错误');
        }

        $search = [];
        $search['mobile_phone'] = $oldNumber;
        $update = $userClient -> where($search) -> update($condition);

        if ($update)
        { // 修改了手机号码，重新刷新session

            $info = $userClient -> where($condition) -> find();

            Session::set('user_login',$info,'front');

            $this -> result(null,1,'修改成功');
        }
    }
}