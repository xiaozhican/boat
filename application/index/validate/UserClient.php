<?php
/**
 * Created by originThink
 * Author: 原点 467490186@qq.com
 * Date: 2016/9/9
 * Time: 15:39
 */

namespace app\index\validate;

use think\Validate;

class UserClient extends Validate
{
    protected $rule = [
        'user_name' => 'require|max:25',
        'password' => 'require|length:6,25'
    ];

    protected $message = [
        'user_name.require' => '用户名不能为空',
        'user_name.length' => '用户名长度2-25位',
        'password.require' => '密码不能为空',
        'password.length' => '密码长度6-25位',
        'code.require' => '验证码不能为空',
        'code.captcha' => '验证码错误'
    ];
}