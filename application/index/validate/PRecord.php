<?php
/**
 * Created by originThink
 * Author: 原点 467490186@qq.com
 * Date: 2016/9/9
 * Time: 15:39
 */

namespace app\index\validate;

use think\Validate;

class PRecord extends Validate
{
    protected $rule = [
        'card_number' => 'require',
        'ship_name' => 'require',
        'tonnage' => 'require',
        'ship_registry' => 'require',
        'register_address' => 'require',
        'now_address' => 'require',
        'owner_name' => 'require',
        'exigency_name' => 'require',
        'exigency_phone' => 'require'
    ];

    protected $message = [
        'card_number.require' => '身份证号不能为空',
        'ship_name.require' => '船名不能为空',
        'tonnage.require' => '吨位不能为空',
        'ship_registry.require' => '船籍不能为空',
        'register_address.require' => '户籍地址不能为空',
        'now_address.require' => '现住地址不能为空',
        'owner_name.require' => '船主名称不能为空',
        'exigency_name.require' => '紧急联系电话不能为空',
        'exigency_phone.require' => '卫星电话不能为空'
    ];
}