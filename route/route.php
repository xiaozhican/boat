<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

/**
 * 前台管理路由
 */


Route::group('/', [
    ''=>'index/Main/home',                                         //首页
])->ext('html');

/**
 * 免权限验证路由
 */
Route::group('index', [
    'index$'=>'index/Index/index',                                         //首页
    'boat'=>'index/Index/boat',                                    //人员备案基本信息
    'idcard'=>'index/Index/idcard',                                    //人员备案上传身份证
    'personal'=>'index/Index/personal',                                    //个人中心
])->ext('html');

/**
 * 后台管理路由
 */

/**
 * 免权限验证路由
 */
Route::group('admin', [
    'login$'=>'admin/Login/login',                                         //登录
    'editPassword'=>'admin/User/editPassword',                             //重置密码
    'logout$'=>'admin/Login/logout',                                       //退出
    'check$'=>'admin/User/check',                                          //验证用户是否存在
    'unlock'=>'admin/Login/unlock',                                        //验证用户是否存在
    'verify'=>'admin/Login/verify',                                        //获取验证码
])->ext('html');
/**
 * 需要权限验证路由
 */
Route::group('admin', [

    //首页
    'index$'=>'admin/Index/index',                                           //首页
    'home'=>'admin/Index/home',                                              //系统信息
    'checkedit'=>'admin/Index/checkedit',//出入港审核或查看
    'homedel'=>'admin/Index/homedel',
    'homereason'=>'admin/Index/homereason',//出入港驳回

    //用户管理
    'userList$'=>'admin/User/userList',                                      //用户列表
    'userInfo$'=>'admin/User/userInfo',                                      //用户信息
    'edit$'=>'admin/User/edit',                                              //添加/编辑用户
    'delete$'=>'admin/User/delete',                                          //删除用户
    'groupList$'=>'admin/User/groupList',                                    //用户组列表
    'editGroup$'=>'admin/User/editGroup',                                    //添加编辑用户组
    'disableGroup$'=>'admin/User/disableGroup',                              //禁用用户组
    'ruleList$'=>'admin/User/ruleList',                                      //用户组规则列表
    'editRule$'=>'admin/User/editRule',                                      //修改用户组规则
    //用户平台管理
    'userMsg'=>'admin/UserPlatform/userMsg',//用户列表
    'msgedit'=>'admin/UserPlatform/msgedit',//用户管理添加和编辑
    'msgedit1'=>'admin/UserPlatform/msgedit1',//
    'msgedit2'=>'admin/UserPlatform/msgedit2',//船主身份证正面照
    'msgedit3'=>'admin/UserPlatform/msgedit3',//船主身份证反面照
    'msgedit4'=>'admin/UserPlatform/msgedit4',//
    'reason'=>'admin/UserPlatform/reason',//驳回
    'usermsgdel'=>'admin/UserPlatform/usermsgdel',//用户删除
    //资讯管理
    'notice'=>'admin/UserPlatform/notice',//资讯列表
    'noticeedit'=>'admin/UserPlatform/noticeedit',//资讯新增和编辑
    'delall'=>'admin/UserPlatform/delall',//批量删除
    'noticedel'=>'admin/UserPlatform/noticedel',//删除
    'uploadnoticeimg'=>'admin/UserPlatform/uploadnoticeimg',//上传公告的图片
    //banner管理
    'banner'=>'admin/UserPlatform/banner',//banner管理
    'delimg'=>'admin/UserPlatform/delimg',//banner删除
    'addbanner'=>'admin/UserPlatform/addbanner',//banner添加
    //公告管理
    'noticegl'=>'admin/UserPlatform/noticegl',//公告管理
    'addnotice'=>'admin/UserPlatform/addnotice',//公告添加
    'delnotice'=>'admin/UserPlatform/delnotice',//公告删除

    //单位成员管理
    'unitUser'=>'admin/UnitUser/unitUser',//单位成员列表
    'unitedit'=>'admin/UnitUser/unitedit',//新增和编辑单位成员
    //操作日志
    'logList'=>'admin/OperationLog/logList',//日志列表

    //系统管理
    'cleanCache$'=>'admin/System/cleanCache',                                //清除缓存
    'log$'=>'admin/System/loginLog',                                         //登录日志
    'downlog$'=>'admin/System/downLoginLog',                                 //下载登录日志
    'menu$'=>'admin/System/menu',                                            //系统菜单
    'editMenu$'=>'admin/System/editMenu',                                    //编辑菜单
    'deleteMenu$'=>'admin/System/deleteMenu',                                //删除菜单
    'config'=>'admin/System/config',                                         //系统配置
    'siteConfig'=>'admin/System/siteConfig',                                 //站点配置
    'noticeConfig'=>'admin/System/noticeConfig',                             //公告配置
    //上传管理
    'upload'=>'admin/Upload/index',                                    //上传图片
])->middleware(app\admin\middleware\CheckAuth::class)->ext('html');          //使用中间件验证


Route::group('home',[
    'home' => 'index/main/home', // 首页
    'entrance' => 'index/index/entrance', // 注册登录页
    'boat'  => 'index/index/boat', // 注册页上传船舶信息
    'idcard' => 'index/index/idcard', // 注册页上传身份证
    'apply' => 'index/apply/index', // 注册页上传身份证
    'person' => 'index/personal/personal', // 个人主页
    'phone' => 'index/personal/phone', // 修改手机号
    'service' => 'index/personal/service', // 客服热线
    'numberchange' => 'index/personal/updatePhone', // 修改手机号
    'updateusername' => 'index/personal/updateUserName', // 更新用户名
    'updateavatar' => 'index/personal/uploadAvatar', // 更新用户名
    'member'    => 'index/index/member', // 船员列表
    'addmember'    => 'index/index/add', // 船员列表
    'memberdel'  => 'index/index/del' //  删除成员
]);


//ajax方法
Route::group('sys',[
    'send' => 'sys/system/send', // 登陆注册发送信息
    'feedback' => 'sys/system/feedback', // 用户反馈
    'getCode' => 'index/personal/getCode'
]);

Route::group('apply',[
    'index' => 'index/apply/index', //船舶出海申报列表
    'delete' => 'index/apply/delete', //船舶出海申报列表
]);

Route::group('user',[
    'register' => 'index/login/register', // 注册
    'login' => 'index/login/login', // 登陆
    'logout' => 'index/login/logout', // 用户退出
]);


/**
 * miss路由
 * 没有定义的路由全部使用该路由
 */
Route::miss('admin/Login/login');
return [
];
